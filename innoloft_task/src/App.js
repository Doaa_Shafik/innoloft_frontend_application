import React from 'react';
import { Switch, Route } from 'react-router-dom';
import PublicTemplates from './public/publicTemplates';
import UserAccount from './public/pages/User';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function App() {
  return (
    <>
      <PublicTemplates>
        <Switch>
          <Route path={'/'} component={UserAccount} />
        </Switch>
      </PublicTemplates>
      <ToastContainer autoClose={3000} />
    </>
  );
}

export default App;
