import React from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import AuthUser from '../../components/User/Auth';
import Profile from '../../components/User/Profile';
import '../../components/User/styles/shared.css';

const UserAccount = ({ auth }) => {
  return (
    <div className="container-account custom-tabs">
      <Tabs>
        <TabList>
          <Tab>Main Information</Tab>
          <Tab>Additional Information</Tab>
        </TabList>
        <TabPanel>
          <Profile />
        </TabPanel>
        <TabPanel>
          <AuthUser />
        </TabPanel>
      </Tabs>
    </div>
  );
};
export default UserAccount;
