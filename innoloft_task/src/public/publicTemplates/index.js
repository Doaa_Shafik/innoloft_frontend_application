/** @jsx jsx */
import { css, jsx } from '@emotion/core';
// eslint-disable-next-line no-unused-vars
import React from 'react';
import Header from '../components/Header';
import Footer from '../components/Footer';
import '../assets/styles/style.css';
import Sidebar from '../components/Sidebar';
const styles = css`
  justify-content: space-evenly;
  @media (min-width: 777px) {
    padding-block-start: 100px;
  }
  @media (max-width: 776px) {
    flex-wrap: wrap;
  }
`;
const PublicTemplates = ({ children }) => {
  return (
    <>
      <Header />
      <div css={styles} className="flex-container wrapper">
        <Sidebar />
        {children}
      </div>
      <Footer />
    </>
  );
};
export default PublicTemplates;
