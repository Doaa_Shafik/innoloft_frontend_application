/** @jsx jsx */
import { css, jsx } from '@emotion/core';
// eslint-disable-next-line no-unused-vars
import React from 'react';
import message from '../../assets/images/chat.svg';
import world from '../../assets/images/world.svg';
import notification from '../../assets/images/notification.svg';
const styles = css`
  flex-wrap: wrap;
  justify-content: space-between;
  align-items: center;
  padding: 5px;
  background: #fff;
  border-bottom: #ddd 1px solid;
  padding: 0 10% 0 calc(10% - 75px);

  .header-icons {
    justify-content: space-between;
    width: 120px;
    img {
      width: 100%;
      max-width: 20px;
    }
  }
  .header-switch-lng {
    align-items: center;
    span {
      padding: 4px;
    }
  }

  @media (max-width: 776px) {
    .header-logo {
      height: 50px;
    }
  }
  @media (min-width: 777px) {
    .header-logo {
      height: 70px;
    }
  }
`;
const Header = () => {
  return (
    <header css={styles} className="flex-container">
      <img
        alt="logo"
        className="header-logo"
        src=" https://innoloft.com/public/wp-content/uploads/2019/09/logo_innoloft.png"
      />
      <ul className="flex-container header-icons">
        <li>
          <img src={message} alt="message-img" />
        </li>
        <li className="flex-container header-switch-lng">
          <img src={world} alt="language-switch" />
          <span>EN</span>
        </li>
        <li>
          <img src={notification} alt="notification-img" />
        </li>
      </ul>
    </header>
  );
};
export default Header;
