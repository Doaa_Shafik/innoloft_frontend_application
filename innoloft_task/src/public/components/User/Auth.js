/** @jsx jsx */
import { css, jsx } from '@emotion/core';
// eslint-disable-next-line no-unused-vars
import React from 'react';
import { Formik, Form, Field } from 'formik';
import axios from 'axios';
import { toast } from 'react-toastify';
import c2v, { Context } from 'c2v';
import notIdentical from '../../assets/images/wrong.png';
import identical from '../../assets/images/strong.png';
import {
  c2vTranslator,
  extractErrors,
  passwordChecker
} from '../../../utils/c2vTranslator';
import { mediumPattern } from './patterns';

const styles = css`
  font-size: 12px;
  padding-inline-start: 13px;
`;

const schema = c2v.obj.requires('password', 'confirm_password', 'email').keys({
  email: c2v.str.email(),
  password: c2v.str.matches(mediumPattern),
  confirm_password: c2v.str.matches(mediumPattern)
});
const validate = async values => {
  const result = await Context.validate(schema, values);
  console.log(result, 'resukt');
  if (result.errors) {
    throw c2vTranslator(result);
  }
};
const init = {
  email: 'aymen@gmail.com',
  password: '12Hh$',
  confirm_password: '12Hh$'
};
class AuthUser extends React.Component {
  SubmitData = values => {
    axios
      .post('https://reqres.in/api/users', values)
      .then(() => {
        toast.success('Your Updated submited Successfuly', {
          position: toast.POSITION.TOP_RIGHT
        });
      })
      .catch(err => {
        toast.error('There is a Problem with updating Try Again', {
          position: toast.POSITION.TOP_RIGHT
        });
      });
  };
  render() {
    return (
      <Formik
        onSubmit={this.SubmitData}
        initialValues={init}
        validate={validate}
      >
        {({ values, errors }) => {
          const message = extractErrors(errors, 'password');
          const indicator = passwordChecker(values);
          return (
            <Form>
              <Field
                name="email"
                component="input"
                type="email"
                className="form-control"
                placeholder={'Update Email'}
              />

              <label className="password-field">
                <Field
                  name="password"
                  component="input"
                  type="password"
                  className="form-control"
                  placeholder={'New Password'}
                />
                <img src={indicator} alt={'password-indicator'} />
              </label>
              {
                <p className="error-password" css={styles}>
                  {message}
                </p>
              }

              <label className="password_confirm-field">
                <Field
                  name="confirm_password"
                  component="input"
                  type="password"
                  className="form-control"
                  placeholder={'Confirm Password'}
                />
                <img
                  src={
                    values.password === values.confirm_password
                      ? identical
                      : notIdentical
                  }
                  alt="indicator-confirm"
                />
              </label>
              <div className="btn-wrapper">
                <button type="submit">Update</button>
              </div>
            </Form>
          );
        }}
      </Formik>
    );
  }
}
export default AuthUser;
