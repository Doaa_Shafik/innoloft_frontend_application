import React from 'react';
import { Formik, Form, Field } from 'formik';
import c2v, { Context } from 'c2v';
import axios from 'axios';
import { toast } from 'react-toastify';
import { c2vTranslator, extractErrors } from '../../../utils/c2vTranslator';
import ReactSelect from 'react-select';

const countries = [
  { label: 'Switzerland', value: 'Switzerland' },
  { label: 'Germany', value: 'Germany' },
  { label: 'Austria', value: 'Austria' }
];
const init = {
  country: 'Germany',
  firstName: 'Ayman',
  lastName: 'Karim',
  address: {
    street: 'ST - 2KB Germany',
    house: 'Soko',
    postalCode: 2536
  }
};

const schema = c2v.obj
  .requires('firstName', 'lastName', 'address', 'country')
  .keys({
    address: c2v.obj.requires('street', 'house', 'postalCode').keys({
      street: c2v.str,
      house: c2v.str,
      postalCode: c2v.num.min(2)
    }),
    country: c2v.str.in('Germany', 'Austria', 'Switzerland'),
    firstName: c2v.str.minLength(2).maxLength(32),
    lastName: c2v.str.minLength(2).maxLength(64)
  });

const validate = async values => {
  const result = await Context.validate(schema, values);
  console.log(c2vTranslator(result), 'tttt');
  if (result.errors) {
    throw c2vTranslator(result);
  }
};

class UserProfile extends React.Component {
  SubmitData = values => {
    axios
      .post('https://reqres.in/api/users', values)
      .then(data => {
        toast.success('Your Updated submited Successfuly', {
          position: toast.POSITION.TOP_RIGHT
        });
      })
      .catch(err => {
        toast.error('There is a Problem with updates Try Again', {
          position: toast.POSITION.TOP_RIGHT
        });
      });
  };
  render() {
    return (
      <Formik
        onSubmit={this.SubmitData}
        initialValues={init}
        validate={validate}
      >
        {({ values, setFieldValue, errors }) => {
          return (
            <Form>
              <Field
                name="firstName"
                component="input"
                type="text"
                label="First Name"
                className="form-control"
                placeholder={'First Name'}
              />
              <div>{extractErrors(errors, 'firstName')}</div>

              <Field
                name="lastName"
                component="input"
                type="text"
                className="form-control"
                placeholder={'Last Name'}
              />
              <div>{extractErrors(errors, 'lastName')}</div>
              <Field
                name="address.street"
                component="input"
                type="text"
                placeholder="Street"
                className="form-control"
              />
              <Field
                name="address.postalCode"
                component="input"
                type="text"
                placeholder="Postal Code"
                className="form-control"
              />
              <Field
                name="address.house"
                component="input"
                type="text"
                placeholder="Home Address"
                className="form-control"
              />
              <ReactSelect
                name="country"
                options={countries}
                getOptionValue={v => v.value}
                getOptionLabel={v => v.label}
                onChange={({ value }) => setFieldValue('country', value)}
                defaultValue={countries[1]}
                className="react-select-container"
                placeholder="select Country"
                classNamePrefix="react-select"
              />
              <div className="btn-wrapper">
                <button type={'submit'}>Update Profile</button>
              </div>
            </Form>
          );
        }}
      </Formik>
    );
  }
}
export default UserProfile;
