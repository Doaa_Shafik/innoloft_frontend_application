/** @jsx jsx */
import { css, jsx } from '@emotion/core';
// eslint-disable-next-line no-unused-vars
import React from 'react';
import { NavLink } from 'react-router-dom';
import home from '../../assets/images/home.svg';
import usr from '../../assets/images/user.svg';
import company from '../../assets/images/company.svg';
import settings from '../../assets/images/settings.svg';
import news from '../../assets/images/news.svg';
import analytics from '../../assets/images/seo-report.svg';
const styles = css`
  img {
    width: 100%;
    max-width: 15px;
    padding-inline-end: 4px;
  }
  a {
    color: #565454;
    padding: 10px 0;
  }
  @media (min-width: 776px) {
    flex-direction: column;
  }
  @media (max-width: 776px) {
    a span {
      display: none;
    }
    a {
      padding: 15px;
    }
    a.active span {
      font-size: 12px;
      text-align: center;
      display: inline-block;
    }
  }
  a.active span {
    color: #e4b302;
  }
`;
const Sidebar = () => {
  return (
    <aside css={styles} className="flex-container sidebar">
      <NavLink exact to="/Home">
        <img src={home} alt="home-img" />
        <span>Home</span>
      </NavLink>

      <NavLink exact to="/">
        <img src={usr} alt="usr-img" />
        <span>User Account</span>
      </NavLink>

      <NavLink to="/company">
        <img src={company} alt="company-img" />
        <span>Company</span>
      </NavLink>

      <NavLink to="/settings">
        <img src={settings} alt="settings-img" />
        <span>Settings</span>
      </NavLink>

      <NavLink to="/news">
        <img src={news} alt="new-img" />
        <span>News</span>
      </NavLink>

      <NavLink to="/reports">
        <img src={analytics} alt="analytics-img" />
        <span>Reports</span>
      </NavLink>
    </aside>
  );
};
export default Sidebar;
