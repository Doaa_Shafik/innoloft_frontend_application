import {
  mediumPattern,
  strongPattern
} from '../public/components/User/patterns';
import wrong from '../public/assets/images/wrong.png';
import medium from '../public/assets/images/warn.png';
import strong from '../public/assets/images/strong.png';
const translate = err => {
  // messages..
  switch (err.rule) {
    case 'object.requires':
    case 'object.requiresIfAny':
      return `required`;
    case 'string.matches':
      return 'Password should contains Uppercase -lowercase letters, numbers and special characters';
    case 'string.minLength':
      return `text is too short`;
    case 'string.maxLength':
      return ` text is too long`;
    case 'number.min':
      return `value is small`;
    case 'string.email':
      return `invalid email address`;
    case 'string.confirmed':
      return `field doesn't match`;
    default:
      return JSON.stringify(err);
  }
};
export const c2vTranslator = result => {
  // override object to contains name of field
  return result.errors.map(err => {
    err.message = translate(err);
    err.name = /"(.+?)"|(\w+)/g.exec(err.dataPath)[0];
    return err;
  });
};
export const extractErrors = (errors, name) => {
  // extract message error depend on field Name

  if (errors.length > 0) {
    const result = errors.filter(err => err.name === name);
    return result.length > 0 ? result[0].message : '';
  }
};

export const passwordChecker = ({ password, confirm_password }) => {
  if (mediumPattern.test(password) && !strongPattern.test(password)) {
    return medium;
  } else if (strongPattern.test(password)) {
    return strong;
  } else if (!mediumPattern.test(password) && !strongPattern.test(password)) {
    return wrong;
  }
};
